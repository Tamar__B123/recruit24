<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Candidate; //פקודה זו מכירה לקונטרולר את הטבלה בעזרת שם המחלקה
use App\User;
use App\Status;
use Illuminate\Support\Facades\Auth; //מאפשר שליפה של היוזר הפעיל
use Illuminate\Support\Facades\Gate; //שימוש בגייטים
use Illuminate\Support\Facades\Session; //



// full name is "App\Http\Controllers\CandidatesController"; 
class CandidatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $candidates = Candidate::all(); //הפקודה הבאה מושכת את כל הנתונים מטבלת המועמדים
        $users = User::all(); //נמשוך את היוזרים ונעביר בקומפקט לויו
        $statuses = Status::all();        
        return view('candidates.index', compact('candidates','users', 'statuses')); //איזה דברים נרצה להעביר ליו
    }

    public function myCandidates() //פונקציה שמפלטרת את המועמדים שלי
    {        
        $userId = Auth::id(); //מספק לנו את האי די של היוזר הפעיל AUTH
        $user = User::findOrFail($userId); //ביוזר יש פונקציה שתביא אותנו לכל המועמדים של יוזר
        $candidates = $user->candidates; //אובייקט שמכיל את כל הקנדידייטס של היוזר הספציפי שלנו -קנדידייטס זוהי פונקציה שמגדירה את הקשר במודל יוזר
        //$candidates = Candidate::all();
        $users = User::all();
        $statuses = Status::all();        
        return view('candidates.index', compact('candidates','users', 'statuses'));
    }

    

    public function changeUser($cid, $uid = null){ // צריכה לדעת באיזה מועמד מדובר והיוזר או0ציונלי 
        Gate::authorize('assign-user'); //הפונקציה אוטוריז מקבלת את שם הגייט
        $candidate = Candidate::findOrFail($cid); //שליפת המועמד מהדאטה בייס
        $candidate->user_id = $uid; // להכניס ליוזר אידי או היו-אידי
        $candidate->save(); //שמירה של המועמד עם האונר שלו
        return back(); //חוזר לדף שהמשתמש היה בו
        //return redirect('candidates');
    }

    public function changeStatus($cid, $sid) //נתונים שנקבל מהלחצן מי המועמד ולאיזה סטטוס הוא יכול לעבור
    {
        $candidate = Candidate::findOrFail($cid); //שליפת המועמד מהדאטה בייס
        if(Gate::allows('change-status', $candidate)) //אם הגייט מרשה לבצע את שינוי הסטטוס למועמד
        { //לפונקציה אלווס נכניס את שם הגייט ומידע נוסף שצריך-המועמד
            $from = $candidate->status->id; //מכניס את הסטטוס הנוכחי של המועמד
            if(!Status::allowed($from,$sid)) return redirect('candidates'); //פונקציה שבודקת האם המעבר תקין במקרה של פאלס       
            $candidate->status_id = $sid; //אם השינוי תקין נשנה את הסטטוס
            $candidate->save(); //שמירת השינוי
        }else{ //כאשר שינוי הסטטוס אסור
            Session::flash('notallowed', 'You are not allowed to change the status of the user becuase you are not the owner of the user'); //הודעה למשתמש 
        }
        return back();
        //return redirect('candidates');
    }          


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() //מובילה אותנו לטופס שמאפשר להוסיף מועמד
    {
        return view('candidates.create'); //שליחה לויו הרלוונטי
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) //שומרת פרטי מועמד חדש
    {
        $candidate = new Candidate(); //אובייקט ריק שיכיל את פרטי הקנדידייט
        //$candidate->name = $request->name;  //לקנדידייט יש את השם מהמודל , לרקוסט יש מהשדה של הטופס
        //$candidate->email = $request->email;
        $can = $candidate->create($request->all()); //שימוש בקיאייט בגלל המס אסיימנט
        $can->status_id = 1; //ברירת מחדל של סטטוס=1 לכל מועמד חדש
        $can->save(); //שמירת הנתונים של הטופס
        return redirect('candidates'); //העברת היוזר לטבלת קנדידייטס שיראה את הוספת המועמד
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) //שולחת אותנו לטופס שמאפשר לערוך מועמד
    {
        $candidate = Candidate::findOrFail($id); //אובייקט שבודק האם האוביידט קיים ומושכת את פרטיו
        return view('candidates.edit', compact('candidate')); //שליחה לויו בעזרת הקומפקט
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) //היעד של הנתונים אחרי שהתקבלו בטופס
    {
       $candidate = Candidate::findOrFail($id); 
       $candidate->update($request->all()); // שמירת הנתונים שעודכנו במס אסיימנט
       return redirect('candidates');   //חזרה לטבלת המועמדים
    }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $candidate = Candidate::findOrFail($id); //בודקת האם קיים מועמד למחיקה
        $candidate->delete(); //מחיקת המועמד
        return redirect('candidates'); //מפנים את המשתמש לטבלת המועמדים
    }
}
