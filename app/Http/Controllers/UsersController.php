<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Department;
use Illuminate\Support\Facades\Auth; //מאפשר שליפה של היוזר הפעיל
use Illuminate\Support\Facades\Gate; //שימוש בגייטים
use Illuminate\Support\Facades\Session; //

class UsersController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $users = User::all();
        return view('users.index', compact('users'));
    }

    public function details($user_id){
        $user = User::findOrFail($user_id);
        $departments = Department::all();
        return view('users.details', compact('user', 'departments'));
    }


    public function changeDepartment(Request $request, $user_id){
        Session::flash('notallowed', 'Cannot change department'); //הודעה למשתמש 
        return back();

    }



    public function makeManager($user_id){
        $user = User::findOrFail($user_id);
        $CurrentUserId = Auth::id(); //מספק לנו את האי די של היוזר הפעיל AUTH
        $CurrentUser = User::findOrFail($CurrentUserId); //ביוזר יש פונקציה שתביא אותנו לכל המועמדים של יוזר
        
        
        if(!$user->isManager() && $CurrentUser->isAdmin()){
            $user->roles()->attach('2');
            $user->save();
            Session::flash('success', 'The role was successfully changed'); //הודעה למשתמש 
        }

        return redirect('users');   
    }

    public function revokeManager($user_id){
        $user = User::findOrFail($user_id);
        $CurrentUserId = Auth::id(); //מספק לנו את האי די של היוזר הפעיל AUTH
        $CurrentUser = User::findOrFail($CurrentUserId); //ביוזר יש פונקציה שתביא אותנו לכל המועמדים של יוזר
        
        if($user->isManager() && $CurrentUser->isAdmin() ){
            $user->roles()->detach('2');
            $user->save();
        }
        return redirect('users');
        
    }


    public function destroy($id)
    {
        $user = User::findOrFail($id); //בודקת האם קיים מועמד למחיקה
        $CurrentUserId = Auth::id(); //מספק לנו את האי די של היוזר הפעיל AUTH
        $CurrentUser = User::findOrFail($CurrentUserId); //ביוזר יש פונקציה שתביא אותנו לכל המועמדים של יוזר

        if($CurrentUser->isAdmin()) {
            $user->delete(); //מחיקת המועמד
        }
        return redirect()->back(); //מפנים את המשתמש לטבלת המועמדים
    }
}
