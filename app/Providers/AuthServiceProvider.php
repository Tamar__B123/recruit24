<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    //כרן נגדיר את הגייט-שערים שיש יוזרים שיכולים או לא יכולים להיכנס לשער שלהם
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('change-status', function ($user, $candidate) {
            return $user->id === $candidate->user_id; // כאן אנו מוודאים שמי שמשנה את הסטטוס למועמד הוא הבעלים של המועמד 
        }); // בודק שיוזר איי די=קנדידייט איי די

        Gate::define('assign-user', function ($user) {
            return $user->isManager(); //בודק שהיוזר הוא מנהל
        });     
    

        //
    }
}
