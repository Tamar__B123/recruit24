<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name', 'email', 'password','department_id', 'role_id',  //השדות שאנחנו מרשים לעדכן במס אסיימנט
    ];

    public function candidates(){
        return $this->hasMany('App\Candidate'); //לכל יוזר יש הרבה משתמשים
    }

    public function department(){
        return $this->belongsTo('App\Department'); //כל יוזר שייך למחלקה
    }  
    //אם היינו קוראים לטבלה 
    //user_roles
    // לא היה צורך לציין את שם הטבלה
    public function roles(){
        return $this->belongsToMany('App\Role','userroles');  // ציון הקשר ושם הטבלה המקשרת      
    }    
    //הפונקציות מחזירות משתנה בוליאני האם מותר או אסור לבצע פעולה מסויימת
    public function isAdmin(){
        $roles = $this->roles; //אלו התפקידים של היוזר
        if(!isset($roles)) return false; //אם אין לו תפקידים- נחזיר פאלס
        foreach($roles as $role){ //נבדור בלולאה האם יש לו את התפקיד אדמין
            if($role->name === 'admin') return true;  //משמעות ה= ה3 הוא שווה מאותו טיייפ-מגביר את אבטחת המידע
        } 
        return false; 
    }

    public function isManager(){
        $roles = $this->roles;
        if(!isset($roles)) return false;
        foreach($roles as $role){
            if($role->name === 'manager') return true; 
        } 
        return false; 
    }


    public function makeManager($user_id){
        $user = User::findOrFail($user_id);
        $user->roles()->attach('manager');
        $user->save();
        return $user;
    }


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

}
