<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) { //הקריאייט יוצר טבלה חדשה
            $table->id(); //כאן נחליט מה הם השדות של טבלת קנדידייטס
            $table->string('name', 20); //בתיעוד של מייגריישן נראה איזה סוגי שדות ניתן ליצור
            $table->string('email', 30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates'); //הפונקציה מבטלת את השינוי שנעשה בפונקציה אפ - מבטלת את הטבלה
    }
}
