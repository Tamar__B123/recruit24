<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserToCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    //לא יכול לקבל ערכים שליליים unsigned
    public function up()
    {
        Schema::table('candidates', function (Blueprint $table) {
            $table->bigInteger('user_id')->unsigned()->nullable()->index()->after('email'); //הוספת עמודה של היוזר שאחראי על הקנדידייט
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('candidates', function (Blueprint $table) {
            $table->dropColumn('user_id'); //ביטול של השדה שהוספנו
        });
    }
}
