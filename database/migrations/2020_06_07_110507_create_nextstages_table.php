<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNextstagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    //יצור שדות לטבלה
    public function up()
    {
        Schema::create('nextstages', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('from')->unsigned()->index; //כאן צריך אינדקס כי אנחנו חייבים את האיי די של הפרום
            $table->bigInteger('to')->unsigned(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nextstages');
    }
}
