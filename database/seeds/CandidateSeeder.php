<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon; //הכרחי לשמירת הזמנים

class CandidateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { //מערך של נתונים שנאכלס איתם את טבלת קנדידייט
        DB::table('candidates')->insert([
            'name' => Str::random(10),
            'email' => Str::random(5).'@gmail.com',
            'created_at' => Carbon::now(), //סיפריית קרבון מאפשרת להכניס את הזמן של עכשיו
            'updated_at' => Carbon::now()
        ]);        
    }
}
