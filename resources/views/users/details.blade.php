@extends('layouts.app')

@section('title', 'User Details')

@section('content')
@if(Session::has('notallowed')) <!--במקרה והגייט מחזיר פאלס-->
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}} <!--הודעה אדומה-->
</div>
@endif
<!--הקישור זה היו-ר-ל של עמוד יצירת המשתמש-->
<h1>User details</h1>




<form method="POST" action="{{ route('details.changeDepartment', $user->id) }}">
                    @csrf
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right"> {{ __('Name: ') . $user->name }}</label>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right"> {{ __('Email: ') . $user->email }}</label>
                    </div>

                    @if (auth()->check())
                        @if (auth()->user()->isAdmin())
                        <div class="form-group row">
                            <label for="department_id" class="col-md-4 col-form-label text-md-right">Department</label>
                            <div class="col-md-6">
                                <select class="form-control" name="department_id">                                                                         
                                   @foreach ($departments as $department)
                                     <option value="{{ $department->id }}" {{ ($user->department->id == $department->id) ? 'selected' : '' }}> 
                                         {{ $department->name }} 
                                     </option>
                                   @endforeach    
                                 </select>
                            </div>
                        </div>
                        <!--סיום-->
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('change department') }}
                                </button>
                            </div>
                        </div>
                        @else
                        <div class="form-group row">
                        <label for="department_name" class="col-md-4 col-form-label text-md-right"> 
                            {{ __('Department: ') . $user->department->name}}</label>
                        </div>
                        @endif
                    @endif
                        
<!--הוספת חלק הבחירה של המחלקות-->
                    </form>

@endsection

