@extends('layouts.app')

@section('title', 'Users')

@section('content')
@if(Session::has('notallowed')) <!--במקרה והגייט מחזיר פאלס-->
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}} <!--הודעה אדומה-->
</div>
@endif
@if(Session::has('success')) <!--במקרה והגייט מחזיר פאלס-->
<div class = 'alert alert-success'>
    {{Session::get('success')}} <!--הודעה אדומה-->
</div>
@endif
<!--הקישור זה היו-ר-ל של עמוד יצירת המשתמש-->
<h1>List of users</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Name</th><th>Email</th><th>Department</th><th>Roles</th><th>Created</th><th>Updated</th>
    </tr>
    <!-- the table data -->
    @foreach($users as $user) <!--המשתנה קנדידייט מוכר כיוון שנשלח מהקונטרולר-->
        <tr>
            <td>{{$user->id}}</td> <!--סוגריים כפולים מסולסלים מדפיסים את המשתנה כמו אקו-->
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>
            {{$user->department->name}}
            </td>
            <td>
            @foreach($user->roles as $role)
                {{$role->name . ","}}
            @endforeach
            </td>
            <td>{{$user->created_at}}</td>
            <td>{{$user->updated_at}}</td>
            <td>
            <a href = "{{route('user.details',$user->id)}}">Details</a><!--קישור ללחיצת על דלית-->
            </td>  
            <td>
            <a href = "{{route('user.delete',$user->id)}}">Delete</a><!--קישור ללחיצת על דלית-->
            </td>  
            @if (auth()->check())
                @if (auth()->user()->isAdmin() && !$user->isManager())
                <td>
                <a href = "{{route('user.makeManager',$user->id)}}">Make Manager</a><!--קישור ללחיצת על דלית-->
                </td>   
                @elseif (auth()->user()->isAdmin() && $user->isManager())
                <td>
                <a href = "{{route('user.removeManger',$user->id)}}">Make not Manager</a><!--קישור ללחיצת על דלית-->
                </td> 
                @endif
            @endif                                                            
        </tr>
    @endforeach
</table>
@endsection

